package com.example.mikloaj.astro.Fragment;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.mikloaj.astro.R;
import com.example.mikloaj.astro.util.AstroProvider;
import com.example.mikloaj.astro.util.AstroObserver;

public abstract class BaseFragment extends Fragment implements AstroObserver {

    private static final String FILE_NAME = "Content";
    protected TextView longitudeV;
    protected TextView latitudeV;
    protected AstroProvider astroProvider = new AstroProvider();
    protected SharedPreferences.Editor editor;
    protected SharedPreferences content;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = getView(inflater, container, savedInstanceState);

        longitudeV = (TextView) view.findViewById(R.id.longitudeV);
        latitudeV = (TextView) view.findViewById(R.id.latitudeV);

        content = getContext().getSharedPreferences(FILE_NAME,Context.MODE_PRIVATE);
        if(content != null){
            editor = content.edit();
            astroProvider = new AstroProvider();
            Float longitude = content.getFloat("longitude",0);
            Float latitude = content.getFloat("latitude",0);
            longitudeV.setText(String.valueOf(getString(R.string.longitude) + " " + longitude));
            latitudeV.setText(String.valueOf(getString(R.string.latitude) + " " + latitude));
            astroProvider.setLocation(longitude,latitude);
            astroProvider.setCurrentDate();
            editor.apply();
            updateData();
        }
        return view;
    }


    public abstract View getView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState);

    @Override
    public void onChangeLocalization(double longitudeValue, double latitudeValue) {
        longitudeV.setText(String.valueOf(getString(R.string.longitude) + " " + longitudeValue));
        latitudeV.setText(String.valueOf(getString(R.string.latitude) + " " + latitudeValue));
        astroProvider.setLocation(longitudeValue, latitudeValue);
        updateData();

    }

    @Override
    public void onChangeTime() {
        astroProvider.setCurrentDate();
        updateData();

    }

    public abstract void updateData();

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        editor.putFloat("longitude",(float)astroProvider.getAstroClient().getLocation().getLongitude());
        editor.putFloat("latitude",(float)astroProvider.getAstroClient().getLocation().getLatitude());
        editor.commit();

    }

}
