package com.example.mikloaj.astro.Fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.mikloaj.astro.R;
import com.example.mikloaj.astro.util.AstroObserver;

import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MoonFragment extends BaseFragment implements AstroObserver {

    @BindView(R.id.moonriseTime)
    TextView moonriseTime;

    @BindView(R.id.moonsetTime)
    TextView moonsetTime;

    @BindView(R.id.newMoon)
    TextView newMoon;

    @BindView(R.id.fullMoon)
    TextView fullMoon;

    @BindView(R.id.ageMoon)
    TextView age;

    @BindView(R.id.illumination)
    TextView illumination;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        return super.onCreateView(inflater, container, savedInstanceState);

    }


    public static MoonFragment newInstance() {

        MoonFragment moonFragment = new MoonFragment();

        return moonFragment;
    }

    @Override
    public View getView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_moon, parent, false);
        ButterKnife.bind(this, view);

        return view;
    }

    @Override
    public void updateData() {
        int length = astroProvider.getAstroClient().getMoonInfo().getMoonrise().toString().length();
        moonriseTime.setText(astroProvider.getAstroClient().getMoonInfo().getMoonrise().toString().substring(0, length - 5));
        moonsetTime.setText(astroProvider.getAstroClient().getMoonInfo().getMoonset().toString().substring(0, length - 5));
        newMoon.setText(astroProvider.getAstroClient().getMoonInfo().getNextNewMoon().toString().substring(0, length - 5));
        fullMoon.setText(astroProvider.getAstroClient().getMoonInfo().getNextFullMoon().toString().substring(0, length - 5));
        age.setText(String.format(Locale.CANADA,"%.2f",astroProvider.getAstroClient().getMoonInfo().getAge()));
        illumination.setText(String.format(Locale.CANADA,"%.2f",astroProvider.getAstroClient().getMoonInfo().getIllumination()));
    }


}
