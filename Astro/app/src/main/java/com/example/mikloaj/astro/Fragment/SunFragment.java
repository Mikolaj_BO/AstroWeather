package com.example.mikloaj.astro.Fragment;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.mikloaj.astro.R;
import com.example.mikloaj.astro.util.AstroObserver;

import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SunFragment extends BaseFragment implements AstroObserver {

    @BindView(R.id.sunriseTime)
    TextView sunriseTime;

    @BindView(R.id.sunriseAzimuth)
    TextView sunriseAzimuth;

    @BindView(R.id.sunsetTime)
    TextView sunsetTime;

    @BindView(R.id.sunsetAzimuth)
    TextView sunsetAzimuth;

    @BindView(R.id.twilightEveningTime)
    TextView twilightEveningTime;

    @BindView(R.id.twilightMorningTime)
    TextView twilightMorningTime;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return super.onCreateView(inflater,container,savedInstanceState);
    }

    public static SunFragment newInstance(){

        SunFragment sunFragment = new SunFragment();

        return sunFragment;
    }

    @Override
    public void updateData() {
        int length = astroProvider.getAstroClient().getSunInfo().getSunrise().toString().length();
        sunriseTime.setText(astroProvider.getAstroClient().getSunInfo().getSunrise().toString().substring(0,length - 5));
        sunriseAzimuth.setText(String.format(Locale.CANADA, "%.2f",astroProvider.getAstroClient().getSunInfo().getAzimuthRise()));
        sunsetTime.setText(astroProvider.getAstroClient().getSunInfo().getSunset().toString().substring(0,length - 5));
        sunsetAzimuth.setText(String.format(Locale.CANADA,"%.2f",astroProvider.getAstroClient().getSunInfo().getAzimuthSet()));
        twilightEveningTime.setText(astroProvider.getAstroClient().getSunInfo().getTwilightEvening().toString().substring(0,length - 5));
        twilightMorningTime.setText(astroProvider.getAstroClient().getSunInfo().getTwilightMorning().toString().substring(0,length - 5));
    }

    @Override
    public View getView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_sun, parent,false);
        ButterKnife.bind(this,view);
        return view;
    }
}
