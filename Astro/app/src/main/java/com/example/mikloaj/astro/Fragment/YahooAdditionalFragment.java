package com.example.mikloaj.astro.Fragment;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.mikloaj.astro.R;
import com.example.mikloaj.astro.WeatherModel.AdditionalWeatherInfo;
import com.example.mikloaj.astro.WeatherModel.BasicWeatherInfo;
import com.example.mikloaj.astro.WeatherModel.Weather;
import com.example.mikloaj.astro.util.AstroObserver;
import com.example.mikloaj.astro.util.YahooWeatherObserver;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;


public class YahooAdditionalFragment extends FragmentInformation implements YahooWeatherObserver {

    private Unbinder unbinder;

    @BindView(R.id.localization)
    TextView localization;

    @BindView(R.id.time)
    TextView time;

    @BindView(R.id.humidityValue)
    TextView humidityValue;

    @BindView(R.id.windSpeedV)
    TextView windSpeedValue;

    @BindView(R.id.windDirectionV)
    TextView windDirectionV;

    @BindView(R.id.visibilityValue)
    TextView visibilityValue;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.additional_yahoo_weather_fragment, container, false);
        unbinder = ButterKnife.bind(this, view);

        return view;
    }



    public static YahooAdditionalFragment newInstance() {

        return new YahooAdditionalFragment();
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onChangeLocalization(Weather weather) {
        AdditionalWeatherInfo additionalWeatherInfo = new AdditionalWeatherInfo(weather);
        localization.setText(additionalWeatherInfo.getLocalization());
        time.setText(additionalWeatherInfo.getTime());
        humidityValue.setText(additionalWeatherInfo.getHumidity());
        windSpeedValue.setText(additionalWeatherInfo.getWindSpeed());
        windDirectionV.setText(additionalWeatherInfo.getWindDirection());
        visibilityValue.setText(additionalWeatherInfo.getVisibility());
    }

    @Override
    public void onChangeTime() {

    }


}
