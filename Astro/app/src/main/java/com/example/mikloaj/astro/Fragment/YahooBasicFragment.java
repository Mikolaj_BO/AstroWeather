package com.example.mikloaj.astro.Fragment;

import android.app.Activity;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.mikloaj.astro.R;
import com.example.mikloaj.astro.WeatherModel.BasicWeatherInfo;
import com.example.mikloaj.astro.WeatherModel.Weather;
import com.example.mikloaj.astro.util.YahooWeatherObserver;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class YahooBasicFragment extends FragmentInformation implements YahooWeatherObserver {

    Unbinder unbinder;

    @BindView(R.id.weatherCode)
    ImageView weatherCodeImage;

    @BindView(R.id.localization)
    TextView localization;

    @BindView(R.id.time)
    TextView time;

    @BindView(R.id.temperatureValue)
    TextView temperatureValue;

    @BindView(R.id.lattitudeValue)
    TextView lattitudeValue;

    @BindView(R.id.longitudeValue)
    TextView longitudeValue;

    @BindView(R.id.pressureValue)
    TextView pressureValue;

    public static YahooBasicFragment newInstance() {

        return new YahooBasicFragment();
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.basic_yahoo_weather_fragment, container, false);
        unbinder = ButterKnife.bind(this, view);

        return view;
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }


    @Override
    public void onChangeLocalization(Weather weather) {
        updateData(weather);
    }

    public void updateData(Weather weather){
        BasicWeatherInfo basicWeatherInfo = new BasicWeatherInfo(weather,getActivity());
        weatherCodeImage.setImageResource(basicWeatherInfo.getImageID());
        localization.setText(basicWeatherInfo.getLocalization());
        time.setText(basicWeatherInfo.getTime());
        temperatureValue.setText(basicWeatherInfo.getTemp());
        pressureValue.setText(basicWeatherInfo.getPressure());
        longitudeValue.setText(basicWeatherInfo.getLongitude());
        lattitudeValue.setText(basicWeatherInfo.getLat());
    }

    @Override
    public void onChangeTime() {

    }

}
