package com.example.mikloaj.astro.Fragment;

import android.app.Activity;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.mikloaj.astro.R;
import com.example.mikloaj.astro.WeatherModel.BasicWeatherInfo;
import com.example.mikloaj.astro.WeatherModel.Forecast;
import com.example.mikloaj.astro.WeatherModel.Weather;
import com.example.mikloaj.astro.util.ForecastAdapter;
import com.example.mikloaj.astro.util.YahooWeatherObserver;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class YahooForecastFragment extends FragmentInformation implements YahooWeatherObserver {

    private Unbinder unbinder;

    private List<Forecast> forecastList = new ArrayList<>();

    @BindView(R.id.forecastList)
    RecyclerView recyclerView;

    private RecyclerView.Adapter adapter;

    public static YahooForecastFragment newInstance() {

        return new YahooForecastFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.forecast_yahoo_fragment, container, false);
        unbinder = ButterKnife.bind(this, view);

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.addItemDecoration(new DividerItemDecoration(getActivity(), LinearLayout.VERTICAL));
        adapter = new ForecastAdapter(forecastList,getActivity());
        recyclerView.setAdapter(adapter);
        recyclerView.setItemAnimator(new DefaultItemAnimator());

        return view;
    }



    @Override
    public void onChangeLocalization(Weather weather) {
        updateData(weather);
    }

    private void updateData(Weather weather){
        forecastList.clear();
        List<Forecast> newData = weather.getQuery().getResults().getChannel().getItem().getForecast();
        forecastList.addAll(newData);
        adapter.notifyDataSetChanged();
        int id = adapter.getItemCount();
    }

    @Override
    public void onChangeTime() {

    }

    @Override
    public void onDetach() {
        super.onDetach();
        unbinder.unbind();
    }

}
