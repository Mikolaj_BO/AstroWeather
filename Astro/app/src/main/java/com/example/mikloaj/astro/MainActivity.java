package com.example.mikloaj.astro;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.SeekBar;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.example.mikloaj.astro.Fragment.MoonFragment;
import com.example.mikloaj.astro.util.AstroObserver;
import com.example.mikloaj.astro.util.Observable;
import com.example.mikloaj.astro.Fragment.SunFragment;
import com.example.mikloaj.astro.util.PageAdapter;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements Observable {

    public static final int MILISECOND = 1000;
    public static final int MINUTE = MILISECOND * 60;

    private List<AstroObserver> observers;
    private List<Fragment> fragmentsViewPager = new ArrayList<>();
    private int refreshTime;
    private double latitudeValue;
    private double longitudeValue;

    protected SharedPreferences.Editor editor;
    protected SharedPreferences content;
    private static final String FILE_NAME = "Content";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_activity_layout);
        setFragmentsViewPager();

        latitudeValue = Double.NaN;
        longitudeValue = Double.NaN;
        refreshTime = MINUTE;
        observers = new ArrayList<>();

        Button setButton = findViewById(R.id.setButton);
        Switch switchActivity = findViewById(R.id.switch1);
        switchActivity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this,YahooWeatherActivity.class);
                startActivity(intent);
            }
        });

        setButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showSettingDialog();
            }
        });

        if (findViewById(R.id.viewPager) != null && findViewById(R.id.viewPagerLandscape) == null) {

            addObserver((AstroObserver) fragmentsViewPager.get(0));
            addObserver((AstroObserver) fragmentsViewPager.get(1));
            ViewPager pager = findViewById(R.id.viewPager);
            PageAdapter pageAdapter = new PageAdapter(getSupportFragmentManager(), fragmentsViewPager);
            pager.setAdapter(pageAdapter);

        } else if (findViewById(R.id.viewPagerLandscape) != null && findViewById(R.id.viewPager) == null) {
            addObserver((AstroObserver) fragmentsViewPager.get(0));
            addObserver((AstroObserver) fragmentsViewPager.get(1));
            ViewPager pager = findViewById(R.id.viewPagerLandscape);
            PageAdapter pageAdapter = new PageAdapter(getSupportFragmentManager(), fragmentsViewPager);
            pager.setAdapter(pageAdapter);
        } else {

            MoonFragment fragmentMoon = new MoonFragment();
            SunFragment fragmentSun = new SunFragment();
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.containerMoon, fragmentMoon)
                    .addToBackStack(null)
                    .commit();
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.containerSun, fragmentSun)
                    .addToBackStack(null)
                    .commit();
            addObserver(fragmentMoon);
            addObserver(fragmentSun);
        }

        content = getSharedPreferences(FILE_NAME, Context.MODE_PRIVATE);
        if (content != null) {
            editor = content.edit();
            this.latitudeValue = content.getFloat("latitude", 0);
            this.longitudeValue = content.getFloat("longitude", 0);
            this.refreshTime = content.getInt("refresh", MINUTE);
            editor.apply();
        }

        if ((!Double.isNaN(longitudeValue) && !Double.isNaN(latitudeValue))) {
            final Handler handler = new Handler();
            final Runnable r = new Runnable() {
                @Override
                public void run() {
                    notifyObserversAboutTime();
                    Log.d("czas", String.valueOf(refreshTime));
                    handler.postDelayed(this, refreshTime); //ms
                }
            };
            handler.postDelayed(r, refreshTime);
        }
    }

    private void showSettingDialog() {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        LayoutInflater inflater = this.getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.menu_layout, null);
        dialogBuilder.setView(dialogView);

        final EditText longitude = dialogView.findViewById(R.id.longitude);
        final EditText latitude = dialogView.findViewById(R.id.latitude);

        if (!Double.isNaN(latitudeValue)) latitude.setText(String.valueOf(latitudeValue));
        if (!Double.isNaN(longitudeValue)) longitude.setText(String.valueOf(longitudeValue));

        final TextView frequent = dialogView.findViewById(R.id.frequency);
        SeekBar seekBar = dialogView.findViewById(R.id.seekBar);
        seekBar.setProgress(refreshTime / MINUTE);
        frequent.setText(String.valueOf(refreshTime / MINUTE));

        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                if (progress == 0) {
                    progress = 1;
                }
                frequent.setText(String.valueOf(progress));
                refreshTime = progress * MINUTE;
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        dialogBuilder.setTitle("Ustawienia");
        dialogBuilder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
            }
        });
        dialogBuilder.setNegativeButton("Wróć", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {

            }
        });
        final AlertDialog alertDialog = dialogBuilder.create();
        alertDialog.show();
        alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String longitudeText = longitude.getText().toString();
                String latitudeText = latitude.getText().toString();
                if (longitudeText.length() < 1 || latitudeText.length() < 1) {
                    makeToast("Nie podales parametrow!");
                }else if(Double.parseDouble(latitudeText) > 90 || Double.parseDouble(longitudeText) > 180){
                    makeToast("Nie poprawne dane!");
                } else {
                    longitudeValue = Double.valueOf(longitude.getText().toString());
                    latitudeValue = Double.valueOf(latitude.getText().toString());
                    notifyObserversAboutLocalization();
                    alertDialog.dismiss();
                }
            }
        });
    }

    public void makeToast(String message){
        Toast toast = Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT);
        toast.show();
    }

    @Override
    public void addObserver(AstroObserver observer) {
        observers.add(observer);
    }

    @Override
    public void removeObserver(AstroObserver observer) {
        observers.remove(observer);
    }

    @Override
    public void notifyObserversAboutLocalization() {
        for (AstroObserver observer : observers) {
            observer.onChangeLocalization(this.longitudeValue, this.latitudeValue);
        }
    }

    @Override
    public void notifyObserversAboutTime() {
        for (AstroObserver observer : observers) {
            observer.onChangeTime();
        }
    }


    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        editor = content.edit();
        editor.putInt("refresh", refreshTime);
        editor.putFloat("longitude", (float) longitudeValue);
        editor.putFloat("latitude", (float) latitudeValue);
        editor.commit();
    }


    public void setFragmentsViewPager() {
        if (fragmentsViewPager.isEmpty()) {
            fragmentsViewPager.add(new MoonFragment());
            fragmentsViewPager.add(new SunFragment());
        }
    }

}
