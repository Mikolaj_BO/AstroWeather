package com.example.mikloaj.astro.Service;

public class ApiUtils {
    public static final String BASE_URL = "https://query.yahooapis.com/v1/public/";

    public static YahooWeatherService getYahooWeatherService() {
        return RetrofitClient.getClient(BASE_URL).create(YahooWeatherService.class);
    }
}
