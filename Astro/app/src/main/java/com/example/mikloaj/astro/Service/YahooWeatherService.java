package com.example.mikloaj.astro.Service;

import com.example.mikloaj.astro.WeatherModel.Weather;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface YahooWeatherService {

    @GET("yql?format=json&env=store%3A%2F%2Fdatatables.org%2Falltableswithkeys")
    Call<Weather> getWeatherInfo(@Query("q") String query);
}
