package com.example.mikloaj.astro.WeatherModel;

public class AdditionalWeatherInfo {
    private String localization;
    private String time;
    private String humidity;
    private String windSpeed;
    private String windDirection;
    private String visibility;
    private Units units;


    public AdditionalWeatherInfo(Weather weather){
        units = weather.getQuery().getResults().getChannel().getUnits();
        localization = weather.getQuery().getResults().getChannel().getLocation().getCity();
        time = weather.getQuery().getResults().getChannel().getLastBuildDate();
        humidity = weather.getQuery().getResults().getChannel().getAtmosphere().getHumidity();
        windSpeed = weather.getQuery().getResults().getChannel().getWind().getSpeed() + units.getSpeed();
        windDirection = weather.getQuery().getResults().getChannel().getWind().getDirection();
        visibility = weather.getQuery().getResults().getChannel().getAtmosphere().getVisibility();
    }

    public String getLocalization() {
        return localization;
    }

    public String getTime() {
        return time;
    }

    public String getHumidity() {
        return humidity;
    }

    public String getWindSpeed() {
        return windSpeed;
    }

    public String getWindDirection() {
        return windDirection;
    }

    public String getVisibility() {
        return visibility;
    }

    public Units getUnits() {
        return units;
    }
}
