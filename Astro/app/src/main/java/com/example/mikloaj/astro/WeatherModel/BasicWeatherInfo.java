package com.example.mikloaj.astro.WeatherModel;

import android.app.Activity;

public class BasicWeatherInfo {

    private String localization;
    private String time;
    private String temp;
    private String lat;
    private String longitude;
    private String pressure;
    private Units units;
    private int imageID;

    public BasicWeatherInfo(Weather weather, Activity activity) {
        units = weather.getQuery().getResults().getChannel().getUnits();
        localization = weather.getQuery().getResults().getChannel().getLocation().getCity();
        time = weather.getQuery().getResults().getChannel().getLastBuildDate();
        temp = weather.getQuery().getResults().getChannel().getItem().getCondition().getTemp() + units.getTemperature();
        lat = weather.getQuery().getResults().getChannel().getItem().getLat();
        longitude = weather.getQuery().getResults().getChannel().getItem().getLong();
        pressure = weather.getQuery().getResults().getChannel().getAtmosphere().getPressure() + units.getPressure();
        setImage(weather.getQuery().getResults().getChannel().getItem().getCondition().getCode(), activity);
    }

    public String getLocalization() {
        return localization;
    }

    public void setLocalization(String localization) {
        this.localization = localization;
    }

    public String getTime() {
        return time;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getPressure() {
        return pressure;
    }

    public void setPressure(String pressure) {
        this.pressure = pressure;
    }

    public Units getUnits() {
        return units;
    }

    public void setUnits(Units units) {
        this.units = units;
    }

    public String getTemp() {
        return temp;
    }

    public String getLat() {
        return lat;
    }

    public int getImageID() {
        return imageID;
    }

    private void setImage(String code, Activity activity) {
        imageID = activity.getResources().getIdentifier("w" + code, "drawable", activity.getPackageName());
    }
}
