package com.example.mikloaj.astro;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.SeekBar;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.example.mikloaj.astro.Fragment.FragmentInformation;
import com.example.mikloaj.astro.Fragment.MoonFragment;
import com.example.mikloaj.astro.Fragment.SunFragment;
import com.example.mikloaj.astro.Fragment.YahooAdditionalFragment;
import com.example.mikloaj.astro.Fragment.YahooBasicFragment;
import com.example.mikloaj.astro.Fragment.YahooForecastFragment;
import com.example.mikloaj.astro.Service.ApiUtils;
import com.example.mikloaj.astro.Service.YahooWeatherService;
import com.example.mikloaj.astro.WeatherModel.Query;
import com.example.mikloaj.astro.WeatherModel.Results;
import com.example.mikloaj.astro.WeatherModel.Weather;
import com.example.mikloaj.astro.util.AstroObserver;
import com.example.mikloaj.astro.util.Observable;
import com.example.mikloaj.astro.util.PageAdapter;
import com.example.mikloaj.astro.util.QueryBuild;
import com.example.mikloaj.astro.util.YahooData;
import com.example.mikloaj.astro.util.YahooObservable;
import com.example.mikloaj.astro.util.YahooWeatherObserver;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class YahooWeatherActivity extends AppCompatActivity implements YahooObservable, FragmentInformation.OnFragmentInteractionListener {

    public static final int MILISECOND = 1000;
    public static final int MINUTE = MILISECOND * 60;

    private List<YahooWeatherObserver> observers = new ArrayList<>();
    private List<Fragment> fragmentsViewPager = new ArrayList<>();
    private ArrayList<String> favPlacesList = new ArrayList<>();
    private Map<String, Weather> cityWithData = new HashMap<>();
    private Weather weather;
    private YahooWeatherService mService;
    private long timeToRefresh = 0;
    private String units;
    private String localization;
    private boolean addToFavourite = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_yahoo_weather);
        initializeFragments();

        Button button = findViewById(R.id.setButtonYahoo);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(checkInternetConnection()){
                    showSettingDialog();
                }else{
                    makeToast("Brak polaczenia z internetem!" +
                            "\nNie mozna dodać nowego miasta!");
                }

            }
        });

        Button button2 = findViewById(R.id.favouritePlaces);
        button2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showFavPlacesDialog();
            }
        });

        Switch switchActivity = findViewById(R.id.switch2);
        switchActivity.setChecked(true);
        switchActivity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(YahooWeatherActivity.this, MainActivity.class);
                startActivity(intent);
            }
        });

        final SwipeRefreshLayout swipeRefreshLayout = findViewById(R.id.swipeToRefresh);

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if(!checkInternetConnection()){
                    makeToast("Brak polączenia z internetem nie mozna odswiezyc!");
                }
                if(localization == null){
                    makeToast("Nie podano żadnej lokalizacji!");
                }
                if(localization != null && checkInternetConnection()){
                    loadWeather(localization,units);
                    makeToast("Zaktualizowane dane!");
                }

                swipeRefreshLayout.setRefreshing(false);
            }
        });

        if (findViewById(R.id.viewPagerYahoo) != null && findViewById(R.id.viewPagerYahooLandscape) == null) {

            ViewPager pager = findViewById(R.id.viewPagerYahoo);
            pager.setOffscreenPageLimit(2);
            PageAdapter pageAdapter = new PageAdapter(getSupportFragmentManager(), fragmentsViewPager);
            pager.setAdapter(pageAdapter);
        }else if(findViewById(R.id.viewPagerYahooLandscape) != null && findViewById(R.id.viewPagerYahoo) == null){
            ViewPager pager = findViewById(R.id.viewPagerYahooLandscape);
            pager.setOffscreenPageLimit(2);
            PageAdapter pageAdapter = new PageAdapter(getSupportFragmentManager(), fragmentsViewPager);
            pager.setAdapter(pageAdapter);
        }else{
            observers = new ArrayList<>();
            YahooBasicFragment fragmentBasic = new YahooBasicFragment();
            YahooAdditionalFragment fragmentAdditional = new YahooAdditionalFragment();
            YahooForecastFragment fragmentForecast = new YahooForecastFragment();
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.containerForecast, fragmentForecast)
                    .addToBackStack(null)
                    .commit();
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.containerBasic, fragmentBasic)
                    .addToBackStack(null)
                    .commit();
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.containerAdditional, fragmentAdditional)
                    .addToBackStack(null)
                    .commit();
            addObserver(fragmentBasic);
            addObserver(fragmentAdditional);
            addObserver(fragmentForecast);
        }

        cityWithData = YahooData.loadData(getApplicationContext());

        if (cityWithData.size() < 1) {
            makeToast("Brak danych w bazie!");
        }else{
            makeToast("Zaladowano zapamietane miasta!");
        }
    }


    private void compareTimeWithCurrentFromMap(String city, String units) {
        Weather weatherForSampleCity;
        if(cityWithData.containsKey(city)){
            weatherForSampleCity = cityWithData.get(city);
        }else{
            loadWeather(city,units);
            return;
        }
        String dateString = weatherForSampleCity.getQuery().getResults().getChannel().getLastBuildDate();
        String[] dataInfo = dateString.split(" ");
        String myDateString = dataInfo[4];

        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
        Date date = null;
        try {
            date = sdf.parse(myDateString);
            Calendar cal = Calendar.getInstance();
            int currentHour = cal.get(Calendar.HOUR);
            cal.setTime(date);
            if (currentHour > (cal.get(Calendar.HOUR) + 2)) {
                loadWeather(weatherForSampleCity.getQuery().getResults().getChannel().getLocation().getCity(), weatherForSampleCity.getQuery().getResults().getChannel().getUnits().getTemperature());
            } else {
                loadWeatherFromDataBase(weatherForSampleCity.getQuery().getResults().getChannel().getLocation().getCity());
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onFragmentInteraction(Fragment fragment) {
        if (fragment instanceof YahooBasicFragment) {
            if (fragmentsViewPager.contains((YahooBasicFragment) fragment)) {
                fragmentsViewPager.remove(fragment);
                removeObserver((YahooWeatherObserver) fragment);
                fragmentsViewPager.add((YahooBasicFragment) fragment);
                addObserver((YahooWeatherObserver) fragment);
            }

        } else if (fragment instanceof YahooAdditionalFragment) {
            if (fragmentsViewPager.contains((YahooAdditionalFragment) fragment)) {
                fragmentsViewPager.remove(fragment);
                removeObserver((YahooWeatherObserver) fragment);
                fragmentsViewPager.add((YahooAdditionalFragment) fragment);
                addObserver((YahooWeatherObserver) fragment);
            }

        } else if (fragment instanceof YahooForecastFragment) {
            if (fragmentsViewPager.contains((YahooForecastFragment) fragment)) {
                fragmentsViewPager.remove(fragment);
                removeObserver((YahooWeatherObserver) fragment);
                fragmentsViewPager.add((YahooForecastFragment) fragment);
                addObserver((YahooWeatherObserver) fragment);
            }
        }

    }

    private void showSettingDialog() {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        LayoutInflater inflater = this.getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.menu_yahoo_weather_layout, null);
        dialogBuilder.setView(dialogView);

        final EditText lozalizationEditText = dialogView.findViewById(R.id.localizationValue);

        final RadioGroup radioGroup = dialogView.findViewById(R.id.radioGroup);

        final CheckBox checkBox = dialogView.findViewById(R.id.checkBox);

        dialogBuilder.setTitle("Ustawienia");
        dialogBuilder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
            }
        });
        dialogBuilder.setNegativeButton("Wróć", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {

            }
        });
        final AlertDialog alertDialog = dialogBuilder.create();
        alertDialog.show();
        alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                localization = lozalizationEditText.getText().toString();
                RadioButton button = dialogView.findViewById(radioGroup.getCheckedRadioButtonId());

                if (lozalizationEditText.length() < 1) {
                    makeToast("Nie podales parametrow!");
                } else {

                    units = button.getText().toString();
                    loadWeather(localization, units);

                    addToFavourite = checkBox.isChecked();

                    alertDialog.dismiss();
                }
            }
        });
    }

    private void showFavPlacesDialog() {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        LayoutInflater inflater = this.getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.menu_yahoo_fav_places, null);
        dialogBuilder.setView(dialogView);

        final ListView list = dialogView.findViewById(R.id.favPlacesList);
        ArrayAdapter<String> adapter;

        loadListFromMap();

        adapter = new ArrayAdapter<String>(this, R.layout.fav_place, favPlacesList);
        list.setAdapter(adapter);

        final Drawable blue = this.getResources().getDrawable(R.drawable.gradient_moon_background, null);
        final Drawable defaultBackground = this.getResources().getDrawable(R.drawable.text_border_background, null);
        final List<String> chosenItems = new ArrayList<>();

        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                TextView v = (TextView) view.findViewById(R.id.Row);

                if (v.getCurrentTextColor() == Color.WHITE) {
                    chosenItems.remove(v.getText().toString());
                    v.setBackground(defaultBackground);
                    v.setTextColor(Color.BLACK);
                } else {
                    v.setBackground(blue);
                    v.setTextColor(Color.WHITE);
                    chosenItems.add(v.getText().toString());
                }
            }
        });

        dialogBuilder.setTitle("Ulubione lokalizacje");

        dialogBuilder.setPositiveButton("Zaladuj pogode", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
            }
        });

        dialogBuilder.setNegativeButton("Usuń zaznaczone", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                for (String i : chosenItems) {
                    favPlacesList.remove(i);
                    cityWithData.remove(i);
                }
                chosenItems.clear();
            }
        });
        final AlertDialog alertDialog = dialogBuilder.create();
        alertDialog.show();

        alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (chosenItems.size() > 1) {
                    makeToast("Mozesz wybrac tylko jedno miasto!");
                } else if (chosenItems.size() == 1) {
                    String cityInfo = chosenItems.get(0);
                    localization = cityWithData.get(cityInfo).getQuery().getResults().getChannel().getLocation().getCity();
                    units = cityWithData.get(cityInfo).getQuery().getResults().getChannel().getUnits().getTemperature();

                    loadWeather(cityWithData.get(cityInfo).getQuery().getResults().getChannel().getLocation().getCity(), cityWithData.get(cityInfo).getQuery().getResults().getChannel().getUnits().getTemperature());

                    alertDialog.dismiss();

                } else {
                    alertDialog.dismiss();
                }
            }
        });
    }

    private void initializeFragments() {
        fragmentsViewPager.add(YahooBasicFragment.newInstance());
        fragmentsViewPager.add(YahooAdditionalFragment.newInstance());
        fragmentsViewPager.add(YahooForecastFragment.newInstance());
        addObserver((YahooWeatherObserver) fragmentsViewPager.get(0));
        addObserver((YahooWeatherObserver) fragmentsViewPager.get(1));
        addObserver((YahooWeatherObserver) fragmentsViewPager.get(2));
    }

    public void makeToast(String message) {
        Toast toast = Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT);
        toast.show();
    }

    public void loadWeather(String city, String units) {

        if (timeToRefresh == 0) {
            timeToRefresh = System.currentTimeMillis();
            if(checkInternetConnection()){
                compareTimeWithCurrentFromMap(city, units);
            }else{
                loadWeatherFromDataBase(city);
                makeToast("Dane mogą być nieaktualne!");
                makeToast("Aby uzyskać aktualne dane polacz z internetem!");
            }

        } else {
            long currentTime = System.currentTimeMillis();
            if ((((currentTime - timeToRefresh) / (1000F * 60F)) > 120 || !cityWithData.containsKey(city)) && checkInternetConnection()) {
                loadWeatherFromYahoo(city, units);
            } else if (((currentTime - timeToRefresh) / (1000F * 60F) < 120 && cityWithData.containsKey(city)) || !checkInternetConnection()) {
                makeToast("Dane mogą być nieaktualne!");
                makeToast("Aby uzyskać aktualne dane polacz z internetem!");
                loadWeatherFromDataBase(city);
            }
        }

    }

    private void loadWeatherFromDataBase(String city) {
        weather = cityWithData.get(city);
        notifyObserversAboutLocalization();
    }

    private void loadWeatherFromDataBase() {
        Object[] values = cityWithData.values().toArray();
        this.weather = (Weather) values[0];
        notifyObserversAboutLocalization();
    }

    private void loadWeatherFromYahoo(String city, String units) {
        mService = ApiUtils.getYahooWeatherService();
        mService.getWeatherInfo(QueryBuild.buildQuery(city, units)).enqueue(new Callback<Weather>() {
            @Override
            public void onResponse(Call<Weather> call, Response<Weather> response) {
                weather = response.body();
                if (weather.getQuery().getResults() != null) {
                    if (weather.getQuery().getResults().getChannel().getAtmosphere() != null) {
                        String chosenCity = weather.getQuery().getResults().getChannel().getLocation().getCity();

                        if (addToFavourite) {
                            cityWithData.put(chosenCity, weather);
                        }
                        notifyObserversAboutLocalization();
                    } else {
                        makeToast("Brak danych dla wprowadzonej lokalizacji!");
                    }
                } else {
                    makeToast("Brak danych dla wprowadzonej lokalizacji!");
                }

            }

            @Override
            public void onFailure(Call<Weather> call, Throwable t) {

            }
        });
    }


    @Override
    public void addObserver(YahooWeatherObserver observer) {
        observers.add(observer);
    }

    @Override
    public void removeObserver(YahooWeatherObserver observer) {
        observers.remove(observer);
    }

    @Override
    public void notifyObserversAboutLocalization() {
        for (YahooWeatherObserver observer : observers) {
            observer.onChangeLocalization(weather);
        }
    }

    @Override
    public void notifyObserversAboutTime() {
        //TODO - changetime
    }

    private void loadListFromMap() {
        favPlacesList.clear();
        favPlacesList.addAll(cityWithData.keySet());
    }

    private boolean checkInternetConnection() {
        ConnectivityManager cm =
                (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return netInfo != null && netInfo.isConnectedOrConnecting();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        YahooData.saveData(cityWithData, getApplicationContext());
        super.onSaveInstanceState(outState);

    }

}
