package com.example.mikloaj.astro.util;

public interface AstroObserver {
    void onChangeLocalization(double longitudeValue, double latitudeValue);
    void onChangeTime();
}
