package com.example.mikloaj.astro.util;

import com.astrocalculator.AstroCalculator;
import com.astrocalculator.AstroDateTime;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;

public class AstroProvider implements Serializable{

    private  AstroCalculator astroClient;

    public AstroProvider() {
        astroClient = new AstroCalculator(new AstroDateTime(),new AstroCalculator.Location(0,0));
        setCurrentDate();
    }

    public void setCurrentDate(){
        Calendar currentDate = Calendar.getInstance();

        AstroDateTime astroDateTime = new AstroDateTime(currentDate.get(Calendar.YEAR), currentDate.get(Calendar.MONTH), currentDate.get(Calendar.DAY_OF_MONTH),
                currentDate.get(Calendar.HOUR_OF_DAY), currentDate.get(Calendar.MINUTE), currentDate.get(Calendar.SECOND),
                currentDate.get(Calendar.ZONE_OFFSET), true);

        astroClient.setDateTime(astroDateTime);
    }

    public void setLocation(double longitude, double latitude){
        astroClient.setLocation(new AstroCalculator.Location(latitude,longitude));
    }

    public AstroCalculator getAstroClient() {
        return astroClient;
    }
}
