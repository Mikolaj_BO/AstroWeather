package com.example.mikloaj.astro.util;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.mikloaj.astro.R;
import com.example.mikloaj.astro.WeatherModel.Forecast;

import java.util.List;

public class ForecastAdapter extends RecyclerView.Adapter<ForecastAdapter.ViewHolder> {

    private List<Forecast> forecastList;
    private Activity activity;

    public static class ViewHolder extends RecyclerView.ViewHolder {

        public TextView dataV,dayV,lowTemp,highTemp;
        public ImageView imageCode;

        public ViewHolder(View v) {
            super(v);
            dataV = v.findViewById(R.id.dataV);
            dayV = v.findViewById(R.id.dayV);
            lowTemp = v.findViewById(R.id.lowTempV);
            highTemp = v.findViewById(R.id.highTempV);
            imageCode = v.findViewById(R.id.codeImage);
        }
    }

    public ForecastAdapter(List<Forecast> forecastList, Activity activity){
        this.forecastList = forecastList;
        this.activity = activity;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Forecast forecast = forecastList.get(position);
        holder.highTemp.setText(forecast.getHigh());
        holder.lowTemp.setText(forecast.getLow());
        holder.dayV.setText(forecast.getDay());
        holder.dataV.setText(forecast.getDate());
        holder.imageCode.setImageResource(forecast.getImageID(activity));
    }

    @Override
    public int getItemCount() {
        return forecastList.size();
    }

    @Override
    public ForecastAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                   int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.forecast_layout, parent, false);

        return new ViewHolder(view);
    }


}
