package com.example.mikloaj.astro.util;

public interface Observable {
    void addObserver(AstroObserver observer);
    void removeObserver(AstroObserver observer);
    void notifyObserversAboutLocalization();
    void notifyObserversAboutTime();
}
