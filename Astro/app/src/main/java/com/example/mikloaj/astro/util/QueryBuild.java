package com.example.mikloaj.astro.util;

import java.util.Locale;

public class QueryBuild {

    public static String buildQuery(String city, String units) {
        StringBuilder query = new StringBuilder();
        query.append("select * from weather.forecast where woeid in (select woeid from geo.places(1) where text=\"");
        query.append(city);
        query.append("\")");
        query.append("and  u=");
        query.append("\"");
        query.append(units);
        query.append("\"");

        return query.toString();
    }

}
