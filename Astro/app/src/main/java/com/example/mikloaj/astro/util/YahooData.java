package com.example.mikloaj.astro.util;

import android.content.Context;
import android.util.Log;

import com.example.mikloaj.astro.WeatherModel.Weather;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.HashMap;
import java.util.Map;

public class YahooData {

    private static final String fileName = "data.bin";

    public static void saveData(Map<String, Weather> cityWithData, Context context){

        try(ObjectOutputStream os = new ObjectOutputStream(context.openFileOutput(fileName, Context.MODE_PRIVATE))){
            os.writeObject(cityWithData);
        }catch(IOException e){
            Log.e(e.getMessage(), "Saving file");
        }

    }

    public static Map<String,Weather> loadData(Context context){
        Map<String,Weather> cityWithData = new HashMap<>();

        try(ObjectInputStream is = new ObjectInputStream(context.openFileInput(fileName))){
            cityWithData = (HashMap)is.readObject();
        }catch(IOException | ClassNotFoundException e){
            Log.e(e.getMessage(), "Saving file");
        }
        return cityWithData;
    }

}
