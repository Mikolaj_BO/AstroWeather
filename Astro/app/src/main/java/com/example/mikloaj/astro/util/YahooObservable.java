package com.example.mikloaj.astro.util;

public interface YahooObservable {
    void addObserver(YahooWeatherObserver observer);
    void removeObserver(YahooWeatherObserver observer);
    void notifyObserversAboutLocalization();
    void notifyObserversAboutTime();
}
