package com.example.mikloaj.astro.util;

import android.app.Activity;

import com.example.mikloaj.astro.WeatherModel.Weather;

public interface YahooWeatherObserver {
    void onChangeLocalization(Weather weather);
    void onChangeTime();
}
